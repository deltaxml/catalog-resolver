// CatalogFixSlashes - A Catalog whose normalization method is overridden
// to provide fixed slashes support (i.e. backslashes converted to
// forward slashes.

/*
 * Licensed to DeltaXML Limited under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The NOTICE file distributed with this work for additional information
 * regarding copyright ownership.
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.xml.resolver;

/**
 * Updated the standard Apache catalog so that it also handles
 * DOS/WINDOWS style path separators, even though they are not supposed to be
 * used within a URI path.
 * 
 * @see org.apache.xml.resolver.Catalog
 * 
 * @author anthonys
 */
public class CatalogFixSlashes extends Catalog {
 
	private boolean debug = false;
	
	/**
	 * Construct a new Catalog
	 */
	public CatalogFixSlashes() {
		if(debug){
			System.err.println("Created CatalogFixSlashes");
		}
	}

	/**
	 * {@inheritDoc} Updated the normalisation so that it fixes DOS/WINDOWS
	 * style path separators to use URI path separators as required. That is
	 * backslashes get converted to forward slashes.
	 * 
	 * @see org.apache.xml.resolver.Catalog#normalizeURI(java.lang.String)
	 */
	protected String normalizeURI(String uriref) {
		String result = super.normalizeURI(fixSlashes(uriref));
		
		if(debug) {
			System.err.println("DEBUG NORMALISE = '"+result+"'");
		}
		
		return result;
	}	
}

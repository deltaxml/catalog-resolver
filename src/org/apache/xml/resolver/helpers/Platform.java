// Platform.java - Some common methods that have platform specific setups.

/*
 * Licensed to DeltaXML Limited under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The NOTICE file distributed with this work for additional information
 * regarding copyright ownership.
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.xml.resolver.helpers;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.SAXParserFactory;

/**
 * The methods in this utility provide a common location for methods
 * that have platform specifid code. In particular, this class was
 * introduced to enable the JAXP mechanisms to be bypassed when
 * this code has been cross-compiled using the IKVM java bytecode to
 * .NET bytecode compiler.
 * 
 * @author anthonys
 *
 */
public class Platform {

  private static boolean bypassJAXP;

  private Platform() {
    bypassJAXP= false;
  }

  /**
   * The unique object representing the platform.
   * Note that all public methods on this object are static, so it does not
   * need to be instanticated.
   */
  public static Platform INSTANCE = new Platform();

  /**
   * Set whether to bypass the JAXP settings.
   */
  public static void setBypassJAXP(boolean bypass) {
     bypassJAXP= bypass;  
  }

  /**
   * Get the SAXParserFactory to use. If the bypassJAXP setting is used reflectively
   * creat an instance of the org.apache.xerces.jaxp.SAXParserFactoryImpl class.
   */
  public static SAXParserFactory getSAXParserFactory() {
    SAXParserFactory spf;

    if (bypassJAXP) {
      try {
        spf= (SAXParserFactory) lookupClass(
          "org.apache.xerces.jaxp.SAXParserFactoryImpl", INSTANCE).newInstance();
      } catch(Exception e) {
        throw new FactoryConfigurationError(
          "Cannot create Apache Xerces SAX parser factory (org.apache.xerces.jaxp.SAXParserFactoryImpl)");
      }
    } else {
      spf= SAXParserFactory.newInstance();
    }
    return spf;
  }

  /**
   * Get the DocumentBuilderFactory to use. If the bypassJAXP setting is used reflectively
   * creat an instance of the org.apache.xerces.jaxp.DocumentBuilderFactoryImpl class.
   */
  public static DocumentBuilderFactory getDocumentBuilderFactory() {
    DocumentBuilderFactory dbf;

    if (bypassJAXP) {
      try {
        dbf= (DocumentBuilderFactory) lookupClass(
          "org.apache.xerces.jaxp.DocumentBuilderFactoryImpl", INSTANCE).newInstance();
      } catch(Exception e) {
        throw new FactoryConfigurationError(
          "Cannot create Apache Xerces document builder factory (org.apache.xerces.jaxp.DocumentBuilderFactoryImpl)");
      }
    } else {
      dbf= DocumentBuilderFactory.newInstance();
    }
    return dbf;
  }

  /**
   * Lookup a class by name. Find the class with the given class name by using
   * the context and current class loaders (in that * order). The first loader
   * to find the class is used to return the result. If neither loader finds 
   * the class a ClassNotFoundException is thrown.
   * 
   * The currentObject can be a Class, this may be useful when the code is
   * called from a static method in which case a class literal based on the
   * current class could be used.
   * 
   * @param className The fully qualified name of the class to lookup.
   * @param currentObject The object that is used to establish the current class
   *   loader.
   * @return The class associated with the given name.
   * @throws ClassNotFoundException If neither the current or context class
   *   loaders can locate the class.
   */
  private static Class<?> lookupClass(String className, Object currentObject)
  throws ClassNotFoundException {
    try {
      return Class.forName(className, true, Thread.currentThread().getContextClassLoader());
    } catch (Throwable t) {
      if (currentObject instanceof Class<?>) {
        return Class.forName(className, true, ((Class<?>)currentObject).getClassLoader());
      } else {
        return Class.forName(className, true, currentObject.getClass().getClassLoader());
      }
    }
  }

}

// UrlResourceHandler.java - Information about public identifiers

/*
 * Licensed to DeltaXML Limited under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * The NOTICE file distributed with this work for additional information
 * regarding copyright ownership.
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.xml.resolver.helpers;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * A URL resource handler enables URL location to be reinterpreted
 * (re-directed). For example, this enables resources within a "resolved"
 * jar file to be loaded without needing to know the location of that jar 
 * file itself.
 * <p>
 * A URL resource handler provides a single method for opening a URL 
 * resource as a stream. The default implementation provides a mechanism to
 * load &quot;internal&quot;resources from either the current ClassLoader
 * or the current thread's context ClassLoader. Here, a resource is loaded
 * by this mechanism if its URL starts with 
 * &quot;{@code jar:file:_internal-jar_._rsc_!/}&quot;. 
 * </p>
 * 
 * @author anthonys
 *
 */
public interface UrlResourceHandler {
  /**
   * The &quot;internal&quot;resource prefix.
   * That is a URL that starts with 
   * &quot;{@code jar:file:_internal-jar_._rsc_!/}&quot;. 
   */
  public static final String INTERNAL_RSC_PREFIX= "jar:file:_internal-jar_._rsc_!/";

  /**
   * Open a URL resource stream, in a possibly customised manner.
   * @param url The URL to open.
   * @return Returns a stream of data from the URL.
   * @throws IOException If the URL location cannot be opened. 
   * Note that missing &quot;internal&quot;resources throw a
   * FileNotFoundException.
   */
  public InputStream openStream(URL url) throws IOException;
  
  /**
   * The default implementation of a URL resource handler.
   * <p>
   * It provides a mechanism to load &quot;internal&quot;resources from
   * either the current ClassLoader or the current thread's context
   * ClassLoader. Here, a resource is loaded by this mechanism if its
   * URL starts with &quot;{@code jar:file:_internal-jar_._rsc_!/}&quot;.
   * </p>
   * <p>
   * Note that missing &quot;internal&quot;resources throw a
   * FileNotFoundException.
   * </p>
   */
  public static final UrlResourceHandler DEFAULT_URL_RESOURCE_HANDLER= new UrlResourceHandler() {
    public InputStream openStream(URL url) throws IOException {
      String name= url.toString();
      InputStream stream= null;
      
      if( name.startsWith(UrlResourceHandler.INTERNAL_RSC_PREFIX) ) {
          ClassLoader cl= getClass().getClassLoader();
          
          name=name.substring(UrlResourceHandler.INTERNAL_RSC_PREFIX.length());
          stream= cl.getResourceAsStream(name);
          
          if( stream == null ) {
            cl= Thread.currentThread().getContextClassLoader();
            stream= cl.getResourceAsStream(name); 
          }
          
          if( stream == null ) {
            throw new FileNotFoundException("Cannot find internal resource file '"+name+"'");
          }
      } else {
        stream= url.openStream();
      }
      return stream;
    }
  };

}
